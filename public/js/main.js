(function () {
    $('form').submit(function (evt){
        evt.preventDefault();

        $.ajax({
            url: '/create',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'link': $('input[name="link"]').val(),
                'link_click_limit': $('input[name="link_click_limit"]').val(),
                'lifetime': $('input[name="lifetime"]').val()
            },
            error: function (request) {
                let errors = request.responseJSON.errors;

                removeError()

                for (const error in errors) {
                    $('.'+error+"__wrapper").find('.invalid-feedback').addClass('d-block').text(errors[error])
                }
            },
            success: function (data) {
                removeError()

                $(".modal .modal-body").text(data['short_link'])
                $(".modal").modal('show');
            }
        })
    })

    function removeError() {
        $('.input__wrapper').each(function (i, item) {
            $(item).find('.invalid-feedback').removeClass('d-block')
        })
    }
})()
