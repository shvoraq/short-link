<?php $__env->startSection('body'); ?>
    <div class="container mt-5">
        <main>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h4 class="mb-3">Short link creating</h4>
                    <form>
                        <div class="row g-3">
                            <div class="col-12 link__wrapper input__wrapper">
                                <label for="firstName" class="form-label">Link</label>
                                <input type="text"
                                       class="form-control"
                                       name="link"
                                       placeholder="https://google.com">
                                <div class="invalid-feedback"></div>
                            </div>

                            <div class="col-12 link_click_limit__wrapper input__wrapper">
                                <label for="lastName" class="form-label">Link click limit</label>
                                <input type="text"
                                       class="form-control"
                                       name="link_click_limit"
                                       placeholder="Maximum number of clicks on the link">
                                <div class="invalid-feedback"></div>
                            </div>

                            <div class="col-12 lifetime__wrapper input__wrapper">
                                <label for="username" class="form-label">Lifetime</label>
                                <input type="time"
                                       class="form-control"
                                       name="lifetime"
                                       placeholder="Link lifetime (hours and minutes)">
                                <div class="invalid-feedback"></div>
                            </div>

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />

                            <button class="w-100 btn btn-primary btn-lg btn-submit">Create short link</button>
                    </form>
                </div>
            </div>
        </main>
    </div>

    <div class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Your short link</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/shortLinks/resources/views/pages/index.blade.php ENDPATH**/ ?>