<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortLinkRequest;
use App\Models\ShortLink;
use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Redirector;
use Illuminate\Http\RedirectResponse;

class ShortLinkController extends Controller
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return response()->view('pages.index');
    }

    /**
     * @param ShortLinkRequest $request
     *
     * @return Application|ResponseFactory|Response
     */
    public function create(ShortLinkRequest $request)
    {
        ShortLink::create($request->only(['link','short_link', 'link_click_limit', 'lifetime']));
        $shortUrl = route('shortLink', $request->get('short_link'));

        return response(['short_link' => $shortUrl], 200);
    }

    /**
     * @param $shortLink string
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function shortLink(string $shortLink)
    {
        $link = ShortLink::where('short_link', $shortLink)
            ->isTimeNotExpired()
            ->isLimitClickNotExpired()
            ->firstOrFail()
            ->decrementLinkClickLimit();

        return redirect($link->link);
    }
}
