<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

/**
 * @property mixed $lifetime
 */
class ShortLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Add to request short_link random string(8)
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'short_link' => Str::random(8)
        ]);
    }

    /**
     * @return void
     */
    public function passedValidation(): void
    {
        $parseLifeTime = explode(':', $this->lifetime);

        $this->merge([
            'lifetime' => Carbon::now()->addHour($parseLifeTime[0])->addMinute($parseLifeTime[1])->toDate()
        ]);
    }

        /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "link"             => "required|url",
            "link_click_limit" => "numeric",
            "lifetime"         => "required|date_format:H:i",
            "short_link"       => "unique:short_links"
        ];
    }
}
