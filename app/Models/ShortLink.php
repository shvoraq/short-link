<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property integer $link_click_limit
 */
class ShortLink extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = ['link', 'short_link', 'link_click_limit', 'lifetime'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['lifetime'];

    /**
     * @param $query Builder
     *
     * @return Builder
     */
    public function scopeIsTimeNotExpired(Builder $query): Builder
    {
        return $query->where('lifetime', '>=', Carbon::now()->toDate());
    }

    /**
     * @param $query Builder
     *
     * @return Builder
     */
    public function scopeIsLimitClickNotExpired(Builder $query): Builder
    {
        return $query->where('link_click_limit', '>=', 0);
    }

    /**
     * Make decrement if link_click_limit not equals zero
     *
     * @return $this
     */
    public function decrementLinkClickLimit(): self
    {
        if ($this->link_click_limit) {
            $decrementCount = 1;

            if ($this->link_click_limit == 1){
                $decrementCount = 2;
            }

            $this->decrement('link_click_limit', $decrementCount);
        }

        return $this;
    }
}
